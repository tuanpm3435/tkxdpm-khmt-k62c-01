package com.rentingbike.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({ RentingBikeBlackBoxTest.class, RentingBikeWhiteBoxTest.class})
public class RentingBikeTestSuice {

}
