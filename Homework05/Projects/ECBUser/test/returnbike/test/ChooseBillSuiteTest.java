package returnbike.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ChooseBillWhiteBoxTest.class, ChooseBillBlackBoxTest.class})
public class ChooseBillSuiteTest {

}
