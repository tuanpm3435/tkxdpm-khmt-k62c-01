package com.ecb.dialog;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.UUID;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.ecb.controller.DockingStationController;
import com.ecb.bean.DockingStation;

@SuppressWarnings("serial")
public class DockingStationDialog extends JDialog {
	private JTextField nameField;
	private JTextField adressField;
	private JTextField freeSpaceField;
	private JTextField totalSpaceField;
	private JTextField phoneField;
	private JTextField listBikeField;

	private GridBagLayout layout;
	private GridBagConstraints c = new GridBagConstraints();
	private DockingStation station;
	
	public DockingStationDialog(DockingStationController controller) {
		super((Frame) null, "Thêm bãi xe", true);
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);

		this.buildControls();

		int row = getLastRowIndex();

		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DockingStation newStation = getNewData();
				if(controller!=null)
					controller.addStation(newStation);
				DockingStationDialog.this.dispose();
			}
		});
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(saveButton, c);

		JButton resetButton = new JButton("Đặt lại");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(resetButton, c);

		this.pack();
		this.setResizable(false);
		this.setVisible(false);
	}

	public DockingStationDialog(DockingStationController controller, DockingStation station) {
		super((Frame) null, "Sửa bãi xe", true);
		this.station = station;
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);

		this.buildControls();

		int row = getLastRowIndex();

		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DockingStation newStation = getNewData();
				if(controller!=null)
					controller.editStation(newStation);
				DockingStationDialog.this.dispose();
			}
		});
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(saveButton, c);

		JButton resetButton = new JButton("Đặt lại");
		resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(resetButton, c);
		this.pack();
		this.setResizable(false);
		this.setVisible(false);
	}

	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
		int rows = dim[1].length;
		return rows;
	}

	public void buildControls() {
		int row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Tên bãi xe");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);

		row = getLastRowIndex();
		JLabel adressLabel = new JLabel("Địa chỉ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(adressLabel, c);
		adressField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(adressField, c);

		row = getLastRowIndex();
		JLabel freeSpaceLabel = new JLabel("Số chỗ trống");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(freeSpaceLabel, c);
		freeSpaceField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		freeSpaceField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent ke) {
				if (ke.getKeyChar() >= '0' && ke.getKeyChar() <= '9' || ke.getKeyCode() == 8) {
					freeSpaceField.setEditable(true);
				} else {
					freeSpaceField.setEditable(false);
				}
			}
		});
		getContentPane().add(freeSpaceField, c);

		row = getLastRowIndex();
		JLabel totalSpaceLabel = new JLabel("Tổng số chỗ trống");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(totalSpaceLabel, c);
		totalSpaceField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		totalSpaceField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent ke) {
				if (ke.getKeyChar() >= '0' && ke.getKeyChar() <= '9' || ke.getKeyCode() == 8) {
					totalSpaceField.setEditable(true);
				} else {
					totalSpaceField.setEditable(false);
				}
			}
		});
		getContentPane().add(totalSpaceField, c);

		row = getLastRowIndex();
		JLabel phoneLabel = new JLabel("SĐT");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(phoneLabel, c);
		phoneField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		phoneField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent ke) {
				if (ke.getKeyChar() >= '0' && ke.getKeyChar() <= '9' || ke.getKeyCode() == 8) {
					phoneField.setEditable(true);
				} else {
					phoneField.setEditable(false);
				}
			}
		});
		getContentPane().add(phoneField, c);

		row = getLastRowIndex();
		JLabel listBikeLabel = new JLabel("Danh sách xe");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(listBikeLabel, c);
		listBikeField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(listBikeField, c);
		if (this.station != null) {
			nameField.setText(this.station.getName());
			adressField.setText(this.station.getAddress());
			freeSpaceField.setText(Integer.toString(this.station.getFreeSpace()));
			totalSpaceField.setText(Integer.toString(this.station.getTotalSpace()));
			phoneField.setText(this.station.getPhone());
			listBikeField.setText("");
		}

	}

	public DockingStation getNewData() {
		DockingStation station = new DockingStation();
		String bikeId;
		if (this.station == null)
			bikeId = UUID.randomUUID().toString();
		else
			bikeId = this.station.getStationId();
		station.setStationId(bikeId);
		station.setName(nameField.getText());
		station.setAddress(adressField.getText());
		station.setFreeSpace(Integer.parseInt(freeSpaceField.getText()));
		station.setTotalSpace(Integer.parseInt(totalSpaceField.getText()));
		station.setPhone((phoneField.getText()));
		// station.setListBike(listBikeField.getText());
		return station;
	}
}
