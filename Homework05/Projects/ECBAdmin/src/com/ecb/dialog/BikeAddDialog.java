package com.ecb.dialog;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.UUID;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.ecb.controller.BikeController;
import com.ecb.bean.Bike;
import com.ecb.bean.DockingStation;
import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class BikeAddDialog extends JDialog{
	
	DockingStation station;
	
	String categories[] = {"Xe đạp đơn thường","Xe đạp đơn điện","Xe đạp đôi thường"};
	
	private JTextField nameField;
	private JComboBox<String> typeField;
	private JTextField weightField;
	private JTextField lisencePlateField;
	private JDateChooser manuafaturingDateField;
	private JTextField manuafaturerField;
	private JTextField costField;
	private JTextField stationField;
	
	private Bike bike;
	private GridBagLayout layout;
	private GridBagConstraints c = new GridBagConstraints();
	
	public BikeAddDialog(DockingStation station, BikeController controller) {
		
        super((Frame) null, "Thêm xe", true);
       	
		this.station = station;
		
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);
	
		this.buildControls();
		
		int row =  getLastRowIndex();
		
		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Bike newBike = getNewData();
				if (newBike!=null) {
					controller.addBike(newBike,station);
					BikeAddDialog.this.dispose();
				}		
			}
		});
		
		
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(saveButton, c);
		
		JButton resetButton = new JButton("Đặt lại");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				nameField.setText("");
//				weightField.setText("");
//				lisencePlateField.setText("");
//				manuafaturerField.setText("");
//				costField.setText("");
			}
		});
		
		
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(resetButton, c);
		
		
		this.pack();
		this.setResizable(false);
		this.setVisible(false);
	}
	
	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Tên xe");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);
		
		row = getLastRowIndex();
		JLabel categoryLabel = new JLabel("Lọai xe");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(categoryLabel, c);
		typeField = new JComboBox<String>(categories);
		typeField.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				Object item = e.getItem();

                if (item.toString().equals(categories[0])) {
                	costField.setText(String.valueOf(400000));
		        } else if (item.toString().equals(categories[1])) {
                	costField.setText(String.valueOf(700000));
		        } else if (item.toString().equals(categories[2])) {
                	costField.setText(String.valueOf(550000));
		        }
			}
		});
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(typeField, c);
		
		row = getLastRowIndex();
		JLabel weightLabel = new JLabel("Trọng lượng");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(weightLabel, c);
		weightField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		weightField.addKeyListener(new KeyAdapter() {
	         public void keyPressed(KeyEvent ke) {
	            if (ke.getKeyChar() >= '0' && ke.getKeyChar() <= '9' || ke.getKeyCode()==8 || ke.getKeyCode()==46) {
	            	weightField.setEditable(true);
	            } else {
	            	weightField.setEditable(false);
	            }
	         }
	      });
		getContentPane().add(weightField, c);
		
		row = getLastRowIndex();
		JLabel lisencePlateLabel = new JLabel("Biển số xe");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(lisencePlateLabel, c);
		lisencePlateField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(lisencePlateField, c);
		
		row = getLastRowIndex();
		JLabel manuafaturingDateLabel = new JLabel("Ngày sản xuất");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(manuafaturingDateLabel, c);
		manuafaturingDateField = new JDateChooser();
		//manuafaturingDateField.setColumns(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(manuafaturingDateField, c);
		
		row = getLastRowIndex();
		JLabel manuafaturerLabel = new JLabel("Nhà sản xuất");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(manuafaturerLabel, c);
		manuafaturerField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(manuafaturerField, c);
		
		row = getLastRowIndex();
		JLabel costLabel = new JLabel("Giá thành");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(costLabel, c);
		costField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		costField.setText(String.valueOf(400000));
		costField.setEditable(false);
		getContentPane().add(costField, c);
		
		row = getLastRowIndex();
		JLabel stationLabel = new JLabel("Bãi xe");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(stationLabel, c);
		stationField = new JTextField(15);
		stationField.setText(station.getName());
		stationField.setEditable(false);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(stationField, c);
	}
	
	public Bike getNewData () {
		if (!nameField.getText().trim().equals("") && !lisencePlateField.getText().trim().equals("") && !manuafaturerField.getText().trim().equals("") && !weightField.getText().trim().equals("") ) {
			bike = new Bike();
			String bikeId = UUID.randomUUID().toString();
			bike.setBikeId(bikeId);
			bike.setName(nameField.getText());
			bike.setLisencePlate(lisencePlateField.getText());
			bike.setManuafaturingDate(manuafaturingDateField.getDate());
			bike.setManuafaturer(manuafaturerField.getText());
			bike.setType(typeField.getSelectedIndex());
			bike.setWeight(Double.parseDouble(weightField.getText()));
			bike.setStationId(station.getStationId());
			bike.setCost(Double.parseDouble(costField.getText()));
			return bike;
		} else {
			JOptionPane.showMessageDialog(BikeAddDialog.this, 
					"Hãy nhập đủ các trường!", 
					"EcoBikeRental", 
					JOptionPane.ERROR_MESSAGE);
			return null;
		}

	}
}
