package com.ecb.dialog;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.ecb.controller.BikeController;
import com.ecb.bean.Bike;
import com.ecb.bean.DockingStation;
import com.toedter.calendar.JDateChooser;

import api.EcoBikeApi;


@SuppressWarnings("serial")
public class BikeEditDialog extends JDialog{
	
	DockingStation station;
	HashMap<String, String> stations;
	
	String categories[] = {"Xe đạp đơn thường","Xe đạp đơn điện","Xe đạp đôi thường"};
	
	private JTextField nameField;
	private JComboBox<String> typeField;
	private JTextField weightField;
	private JTextField lisencePlateField;
	private JDateChooser manuafaturingDateField;
	private JTextField manuafaturerField;
	private JTextField costField;
	private JComboBox comboBox;
	
	private Bike bike;
	private GridBagLayout layout;
	private GridBagConstraints c = new GridBagConstraints();
	
	public BikeEditDialog(DockingStation station,Bike bike, BikeController controller) {
		
        super((Frame) null, "Sửa thông tin xe", true);
       	
		this.station = station;
		
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);
	
		this.buildControls(bike);
		
		int row =  getLastRowIndex();
		
		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Bike newBike = getNewData();
				newBike.setBikeId(bike.getBikeId());
				controller.editBike(newBike,station);
				BikeEditDialog.this.dispose();
			}
		});
		
		
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(saveButton, c);
		
		JButton resetButton = new JButton("Đặt lại");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				nameField.setText("");
//				weightField.setText("");
//				lisencePlateField.setText("");
//				manuafaturerField.setText("");
//				costField.setText("");
			}
		});
		
		
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(resetButton, c);
		
		
		this.pack();
		this.setResizable(false);
		this.setVisible(false);
	}
	
	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	
	public void buildControls(Bike bike) {
		int row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Tên xe");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);
		
		row = getLastRowIndex();
		JLabel categoryLabel = new JLabel("Lọai xe");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(categoryLabel, c);
		typeField = new JComboBox<String>(categories);
		typeField.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				Object item = e.getItem();

                if (item.toString().equals(categories[0])) {
                	costField.setText(String.valueOf(400000));
		        } else if (item.toString().equals(categories[1])) {
                	costField.setText(String.valueOf(700000));
		        } else if (item.toString().equals(categories[2])) {
                	costField.setText(String.valueOf(550000));
		        }
			}
		});
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(typeField, c);
		
		row = getLastRowIndex();
		JLabel weightLabel = new JLabel("Trọng lượng");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(weightLabel, c);
		weightField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		weightField.addKeyListener(new KeyAdapter() {
	         public void keyPressed(KeyEvent ke) {
	            if (ke.getKeyChar() >= '0' && ke.getKeyChar() <= '9' || ke.getKeyCode()==8 || ke.getKeyCode()==46) {
	            	weightField.setEditable(true);
	            } else {
	            	weightField.setEditable(false);
	            }
	         }
	      });
		getContentPane().add(weightField, c);
		
		row = getLastRowIndex();
		JLabel lisencePlateLabel = new JLabel("Biển số xe");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(lisencePlateLabel, c);
		lisencePlateField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(lisencePlateField, c);
		
		row = getLastRowIndex();
		JLabel manuafaturingDateLabel = new JLabel("Ngày sản xuất");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(manuafaturingDateLabel, c);
		manuafaturingDateField = new JDateChooser();
		//manuafaturingDateField.setColumns(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(manuafaturingDateField, c);
		
		row = getLastRowIndex();
		JLabel manuafaturerLabel = new JLabel("Nhà sản xuất");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(manuafaturerLabel, c);
		manuafaturerField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(manuafaturerField, c);
		
		row = getLastRowIndex();
		JLabel costLabel = new JLabel("Giá thành");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(costLabel, c);
		costField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		costField.setText(String.valueOf(400000));
		costField.setEditable(false);
		getContentPane().add(costField, c);
		
		stations = new HashMap<String, String>();
		ArrayList<DockingStation> stationsRes = EcoBikeApi.getInstance().getStation(null);
		int selectIndex = 0;
		for (int i = 0;i<stationsRes.size();i++) {
			DockingStation s= stationsRes.get(i);
			stations.put(s.getName(), s.getStationId());
			if (s.getStationId().equals(station.getStationId())) {
				selectIndex = i;
			}
		}
		
		row = getLastRowIndex();
		JLabel stationLabel = new JLabel("Bãi xe");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(stationLabel, c);
        comboBox = new JComboBox();
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(comboBox,c);
		comboBox.setModel(new DefaultComboBoxModel(stations.keySet().toArray()));
		comboBox.setSelectedIndex(selectIndex);
		
		if (bike != null) {
			nameField.setText(bike.getName());
			lisencePlateField.setText(bike.getLisencePlate());
			manuafaturingDateField.setDate(bike.getManuafaturingDate());
			manuafaturerField.setText(bike.getManuafaturer());
			weightField.setText(String.valueOf(bike.getWeight()));
			typeField.setSelectedIndex(bike.getType());
			costField.setText(String.valueOf(bike.getCost()));
		}
	}
	
	public Bike getNewData () {
		bike = new Bike();
		bike.setName(nameField.getText());
		bike.setLisencePlate(lisencePlateField.getText());
		bike.setManuafaturingDate(manuafaturingDateField.getDate());
		bike.setManuafaturer(manuafaturerField.getText());
		bike.setType(typeField.getSelectedIndex());
		bike.setWeight(Double.parseDouble(weightField.getText()));
		bike.setStationId(stations.get(comboBox.getSelectedItem()));
		bike.setCost(Double.parseDouble(costField.getText()));
		return bike;
	}
}
