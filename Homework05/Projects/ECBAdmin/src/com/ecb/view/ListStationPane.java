package com.ecb.view;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.ecb.controller.AdminBikePageController;
import com.ecb.bean.DockingStation;

@SuppressWarnings("serial")
public class ListStationPane extends JPanel {
	
	private AdminBikePageController controller;
	
	protected GridBagLayout layout;
	protected GridBagConstraints c;
	
	private DockingStation temp;
	private ArrayList<DockingStation> dockingStations;
	private ArrayList<JButton> stationBtn;

	public ListStationPane(AdminBikePageController adminBikePageController) {
		controller = adminBikePageController;
		stationBtn = new ArrayList<JButton>();
		
		layout = new GridBagLayout();
		this.setLayout(layout);
		
		c = new GridBagConstraints();
		c.insets = new Insets(5, 5, 15, 5);
		c.fill = GridBagConstraints.HORIZONTAL;
		
		JLabel title = new JLabel("Danh s�ch b�i xe");
		title.setFont(new Font("Serif", Font.PLAIN, 22));
		addComponent(title, 0, 1, 4, 4);
	}
	
	public void addComponent(Component comp, int row, int col, int nrow, int ncol)
	{
		c.insets = new Insets(5, 5, 15, 5);
		c.ipady = 15;
		c.gridx=col;
		c.gridy=row;

		c.gridwidth=ncol;
		c.gridheight=nrow;

		layout.setConstraints(comp,c);
		add(comp);
	}
	
	public void getListStation() {
		if (dockingStations != null) {
			for (int i = 0;i< dockingStations.size();i++) {
				JButton station = new JButton(dockingStations.get(i).getName());
				stationBtn.add(station);
				addComponent(station, getLastRowIndex(), 1, 2, 4);
				
				station.addActionListener(new ActionListener() {		
					@Override
					public void actionPerformed(ActionEvent e) {
						for (int j = 0;j<dockingStations.size();j++) {
							if (station.getText().toString().equals(dockingStations.get(j).getName())) {
								temp = dockingStations.get(j);
								break;
							}
						}
						controller.selectStation(temp);
					}
				});
			}
		}
	}
	public int getLastRowIndex() {
		layout.layoutContainer(this);
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}

	public ArrayList<DockingStation> getDockingStations() {
		return dockingStations;
	}

	public void setDockingStations(ArrayList<DockingStation> dockingStations) {
		this.dockingStations = dockingStations;
	}
	
	
}
