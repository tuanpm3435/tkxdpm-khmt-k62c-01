package com.ecb.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.ecb.controller.AdminBikePageController;
import com.ecb.bean.Bike;
import com.ecb.bean.DockingStation;

@SuppressWarnings("serial")
public class TableBikePane extends JPanel{
	
	DockingStation station;
	
	private AdminBikePageController controller;
	
	 private JButton addBikeBtn;
	 private JButton editBikeBtn;
	 private JButton delBikeBtn;
	 private JLabel stationName;
	 private JScrollPane jScrollPaneBikeTable;
	 private JTable bikeTable;

	 private String [] columnNames = new String [] {
	            "ID", "Tên xe", "Loại xe", "Trọng lượng", "Biển số xe","Ngày sản xuất","Nhà sản xuất","Giá thành"};
	 private Object data = new Object [][] {};
	 private ArrayList<Bike> listBike;
	public TableBikePane (AdminBikePageController adminBikePageController) {
		controller = adminBikePageController;
		
		Border margin = new EmptyBorder(10, 10, 10, 10);
		this.setBorder(BorderFactory.createRaisedBevelBorder());
		Border border = this.getBorder();
		this.setBorder(BorderFactory.createCompoundBorder(border, margin));

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        stationName = new JLabel("Công viên Thống Nhất");
        stationName.setFont(new Font("Serif", Font.PLAIN + Font.BOLD, 18));
        stationName.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        add(stationName);
        
        JPanel searchPanel = new JPanel();
        searchPanel.add(Box.createRigidArea(new Dimension(650,0)));
        JButton searchBtn = new JButton("Tìm kiếm");
        searchPanel.add(searchBtn);
        JTextField searchField = new JTextField();
        searchField.setPreferredSize(new Dimension(180,25));
        searchPanel.add(searchField);
        searchPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        add(searchPanel);
         
        //add(Box.createRigidArea(new Dimension(0, 15)));
        
        jScrollPaneBikeTable = new JScrollPane();
        bikeTable = new JTable();
        bikeTable.setModel(new DefaultTableModel((Object[][]) data, columnNames));
        TableColumn column = null;
        for (int i = 0; i < 8; i++) {
            column = bikeTable.getColumnModel().getColumn(i);
            if (i == 0) {
                column.setPreferredWidth(15);
            } else if (i == 2 || i == 5){
                column.setPreferredWidth(50);
            } else if (i==6){
                column.setPreferredWidth(60);
            } else {
            	column.setPreferredWidth(30);
            }
        }
        jScrollPaneBikeTable.setViewportView(bikeTable);
        bikeTable.setFillsViewportHeight(true);
       // jScrollPaneBikeTable.setPreferredSize(new Dimension (1000, 300));
        jScrollPaneBikeTable.setAlignmentX(JPanel.LEFT_ALIGNMENT);
        add(jScrollPaneBikeTable);
        
        add(Box.createRigidArea(new Dimension(0, 15)));
        
        JPanel panel = new JPanel();
        FlowLayout flowLayout = new FlowLayout();
        flowLayout.setHgap(150);
        panel.setLayout(flowLayout);
        addBikeBtn = new JButton("Thêm xe mới");
        addBikeBtn.setPreferredSize(new Dimension(150,25));
        panel.add(addBikeBtn);
        addBikeBtn.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.addNewBike(station);
			}
		});
        editBikeBtn = new JButton("Sửa thông tin xe");
        editBikeBtn.setPreferredSize(new Dimension(150,25));
        panel.add(editBikeBtn);
        editBikeBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				 int selectRow = bikeTable.getSelectedRow();
	             if(selectRow != -1)
	                 controller.editBike(station, listBike.get(selectRow));

			}
		});
        delBikeBtn = new JButton("Xóa xe");
        delBikeBtn.setPreferredSize(new Dimension(150,25));
        panel.add(delBikeBtn);
        panel.setAlignmentX(Component.LEFT_ALIGNMENT);
        add(panel);
	}
	
	public void showListBikes(ArrayList<Bike> list) {
		this.listBike = list;
        int size = list.size();
        Object [][] bikes = new Object[size][8];
        for (int i = 0; i < size; i++) {
           bikes[i][0] = list.get(i).getBikeId();
           bikes[i][1] = list.get(i).getName();
           int type = list.get(i).getType();
           if (type == 0) {
        	   bikes[i][2] = "Xe đạp đơn thường";
           } else if (type == 1) {
        	   bikes[i][2] = "Xe đạp đơn điện";
           } else {
        	   bikes[i][2] = "Xe đạp đôi thường";
           }
           bikes[i][3] = list.get(i).getWeight();
           bikes[i][4] = list.get(i).getLisencePlate();
           DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy"); 
           bikes[i][5] = dateFormat.format(list.get(i).getManuafaturingDate());
           bikes[i][6] = list.get(i).getManuafaturer();
           bikes[i][7] = list.get(i).getCost();
        }
        bikeTable.setModel(new DefaultTableModel(bikes, columnNames));
    }
	
	public void setStationName(String name) {
		stationName.setText(name);
	}

	public DockingStation getStation() {
		return station;
	}

	public void setStation(DockingStation station) {
		this.station = station;
	}
	
}
