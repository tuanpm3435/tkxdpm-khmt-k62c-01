package com.ecb.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.Box;
import javax.swing.table.TableColumn;

import com.ecb.controller.AdminDockingStationController;
import com.ecb.bean.DockingStation;

public class TableDockingStationPane extends JPanel {
    public static final String[] columnNames = { "ID", "Tên bãi xe", "Địa chỉ", "Số chỗ trống", "Tổng số chỗ trống",
            "SĐT", "Danh sách xe trong bãi" };
    private Object data = new Object[][] {};
    private AdminDockingStationController controller;
    private JButton addBikeBtn;
    private JButton editStationBtn;
    private JButton delBikeBtn;
    private JScrollPane jScrollPaneDokingStationTable;
    private JTable dockingStationTable;
    private ArrayList<DockingStation> listDockingStation;
    

    public TableDockingStationPane(AdminDockingStationController controller) {
        this.controller = controller;

        //this code will create a table and search input
        Border margin = new EmptyBorder(10, 10, 10, 10);
        this.setBorder(BorderFactory.createRaisedBevelBorder());
        Border border = this.getBorder();
        this.setBorder(BorderFactory.createCompoundBorder(border, margin));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JPanel searchPanel = new JPanel();
        searchPanel.add(Box.createRigidArea(new Dimension(850, 0)));
        JButton searchBtn = new JButton("Tìm kiếm");
        searchPanel.add(searchBtn);
        JTextField searchField = new JTextField();
        searchField.setPreferredSize(new Dimension(180, 25));
        searchPanel.add(searchField);
        searchPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        add(searchPanel);

        // add(Box.createRigidArea(new Dimension(0, 15)));

        jScrollPaneDokingStationTable = new JScrollPane();
        dockingStationTable = new JTable();
        dockingStationTable.setModel(new DefaultTableModel((Object[][]) data, columnNames));
        TableColumn column = null;
        for (int i = 0; i < columnNames.length; i++) {
            column = dockingStationTable.getColumnModel().getColumn(i);
            if (i == 0) {
                column.setPreferredWidth(15);
            } else if (i == 2 || i == 5) {
                column.setPreferredWidth(50);
            } else if (i == 6) {
                column.setPreferredWidth(60);
            } else {
                column.setPreferredWidth(30);
            }
        }
        jScrollPaneDokingStationTable.setViewportView(dockingStationTable);
        dockingStationTable.setFillsViewportHeight(true);
        // jScrollPaneBikeTable.setPreferredSize(new Dimension (1000, 300));
        jScrollPaneDokingStationTable.setAlignmentX(JPanel.LEFT_ALIGNMENT);
        add(jScrollPaneDokingStationTable);

        add(Box.createRigidArea(new Dimension(0, 15)));

        JPanel panel = new JPanel();
        FlowLayout flowLayout = new FlowLayout();
        flowLayout.setHgap(150);
        panel.setLayout(flowLayout);
        addBikeBtn = new JButton("Thêm bãi xe mới");
        addBikeBtn.setPreferredSize(new Dimension(150, 25));
        panel.add(addBikeBtn);
        addBikeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.addNewDockingStation();
            }
        });
        editStationBtn = new JButton("Sửa thông tin bãi xe");
        editStationBtn.setPreferredSize(new Dimension(150, 25));
        editStationBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectRow = dockingStationTable.getSelectedRow();
                if(selectRow != -1)
                      controller.editDockingStation(listDockingStation.get(selectRow));

            }
        });
        panel.add(editStationBtn);
        delBikeBtn = new JButton("Xóa xe");
        delBikeBtn.setPreferredSize(new Dimension(150, 25));
        delBikeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectRow = dockingStationTable.getSelectedRow();
                if(selectRow !=-1)
                 controller.editDockingStation(listDockingStation.get(selectRow));
            }
        });
        panel.add(delBikeBtn);
        panel.setAlignmentX(Component.LEFT_ALIGNMENT);
        add(panel);
    };

    public void showListDockingStation(ArrayList<DockingStation> list) {
        this.listDockingStation = list;
        int size = list.size();
        Object [][] stations = new Object[size][8];
        for (int i = 0; i < size; i++) {
           stations[i][0] = list.get(i).getStationId();
           stations[i][1] = list.get(i).getName();
           stations[i][2] = list.get(i).getAddress();
           stations[i][3] = list.get(i).getFreeSpace();
           stations[i][4] = list.get(i).getTotalSpace();
           stations[i][5] = list.get(i).getPhone();
           stations[i][6] = list.get(i).getListBike();
        }
        dockingStationTable.setModel(new DefaultTableModel(stations, columnNames));
    }

}
