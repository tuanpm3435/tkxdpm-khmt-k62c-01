package com.ecb.view;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

@SuppressWarnings("serial")
public class AdminBikePagePane extends JPanel {

	public AdminBikePagePane(ListStationPane listStationPane,TableBikePane tableBikePane) {
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
			
		this.add(listStationPane);
		this.add(tableBikePane);
		
		
		layout.putConstraint(SpringLayout.WEST, listStationPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, listStationPane, 5, SpringLayout.NORTH, this);
		
		
		layout.putConstraint(SpringLayout.WEST, tableBikePane, 10, SpringLayout.EAST, listStationPane);
		layout.putConstraint(SpringLayout.NORTH, tableBikePane, 5, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.EAST, tableBikePane, -10, SpringLayout.EAST, this);
	}
}
