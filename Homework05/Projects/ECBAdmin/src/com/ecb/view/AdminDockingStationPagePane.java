package com.ecb.view;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

public class AdminDockingStationPagePane extends JPanel {
    public AdminDockingStationPagePane(TableDockingStationPane tableDockingStation) {
		SpringLayout layout = new SpringLayout();
        this.setLayout(layout);	
		this.add(tableDockingStation);	
		
		layout.putConstraint(SpringLayout.WEST, tableDockingStation, 10, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, tableDockingStation, 5, SpringLayout.NORTH, this); 
	}
}
