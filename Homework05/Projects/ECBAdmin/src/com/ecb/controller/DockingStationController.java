package com.ecb.controller;
import com.ecb.bean.DockingStation;

import api.EcoBikeApi;

public class DockingStationController {
	private AdminDockingStationController adminController;
	public DockingStationController( AdminDockingStationController adminController)
	{
		this.adminController = adminController;
	}

    public void addStation(DockingStation station) {
    	EcoBikeApi.getInstance().addDockingStation(station);
		this.adminController.showAllData();
	}
	public void editStation(DockingStation station){
		EcoBikeApi.getInstance().editDockingStation(station);
		this.adminController.showAllData();
	}
}
