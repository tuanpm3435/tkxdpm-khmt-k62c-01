package com.ecb.controller;

import com.ecb.bean.Bike;
import com.ecb.bean.DockingStation;

import api.EcoBikeApi;

public class BikeController {
	
	private AdminBikePageController adminBikePageController;
	
	public BikeController(AdminBikePageController adminBikePageController) {
		super();
		this.adminBikePageController = adminBikePageController;
	}
	
	public void addBike(Bike bike,DockingStation station) {
		EcoBikeApi.getInstance().addBike(bike);
		adminBikePageController.selectStation(station);
	}
	
	public void editBike(Bike bike,DockingStation station) {
		EcoBikeApi.getInstance().editBike(bike);
		adminBikePageController.selectStation(station);
	}
}
