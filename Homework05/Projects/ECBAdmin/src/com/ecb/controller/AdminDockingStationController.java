package com.ecb.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.swing.JPanel;

import com.ecb.dialog.DockingStationDialog;
import com.ecb.view.AdminDockingStationPagePane;
import com.ecb.view.TableDockingStationPane;
import com.ecb.bean.DockingStation;

import api.EcoBikeApi;

public class AdminDockingStationController {

    private AdminDockingStationPagePane pagePane;

    private TableDockingStationPane tableDockingStation;
    public AdminDockingStationController() {
		super();
		tableDockingStation = new TableDockingStationPane(this);
		this.showAllData();
		// tableDockingStation.setStationName(stations.get(0).getName());
		pagePane = new AdminDockingStationPagePane(tableDockingStation);
		

	}
	public void showAllData()
	{
		this.tableDockingStation.showListDockingStation(this.getAllDockingStation());
	} 
	public JPanel getDataPagePane() {
		return pagePane;
	}

	public ArrayList<DockingStation> getStation(Map<String, String> searchParams) {
		return EcoBikeApi.getInstance().getStation(searchParams);
	}
	
	public ArrayList<DockingStation> getAllDockingStation() {
		return EcoBikeApi.getInstance().getStation(null);
	}	
	
	public void addNewDockingStation () {
		DockingStationDialog dialog = new DockingStationDialog(new DockingStationController(this));
		dialog.setVisible(true);
	}
	public void editDockingStation (DockingStation station) {
		DockingStationDialog dialog = new DockingStationDialog(new DockingStationController(this), station);
		dialog.setVisible(true);
	}
}
