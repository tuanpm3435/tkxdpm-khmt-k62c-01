package com.ecb.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.swing.JPanel;

import com.ecb.dialog.BikeAddDialog;
import com.ecb.dialog.BikeEditDialog;
import com.ecb.view.AdminBikePagePane;
import com.ecb.view.ListStationPane;
import com.ecb.view.TableBikePane;
import com.ecb.bean.Bike;
import com.ecb.bean.DockingStation;

import api.EcoBikeApi;

public class AdminBikePageController {
	
	private AdminBikePagePane pagePane;
	
	private TableBikePane tableBikePane;

	public AdminBikePageController() {
		super();
		
		ListStationPane listStationPane = new ListStationPane(this);
		ArrayList<DockingStation> stations = this.getStation(null);
		listStationPane.setDockingStations(stations);
		listStationPane.getListStation();
		
		tableBikePane = new TableBikePane(this);
		tableBikePane.setStation(stations.get(0));
		tableBikePane.showListBikes(this.getBike(stations.get(0).getStationId()));
		tableBikePane.setStationName(stations.get(0).getName());
		
		pagePane = new AdminBikePagePane(listStationPane, tableBikePane);

	}
	
	public JPanel getDataPagePane() {
		return pagePane;
	}

	public ArrayList<DockingStation> getStation(Map<String, String> searchParams) {
		return EcoBikeApi.getInstance().getStation(searchParams);
	}
	
	public ArrayList<Bike> getBike(String stationId) {
		return EcoBikeApi.getInstance().getBikes(stationId);
	}
	
	public void selectStation (DockingStation station) {
		tableBikePane.setStation(station);
		tableBikePane.showListBikes(this.getBike(station.getStationId()));
		tableBikePane.setStationName(station.getName());
	}
	
	public void addNewBike (DockingStation station) {
		BikeAddDialog dialog = new BikeAddDialog(station,new BikeController(this));
		dialog.setVisible(true);
	}
	public void editBike (DockingStation station,Bike bike) {
		BikeEditDialog dialog = new BikeEditDialog(station,bike,new BikeController(this));
		dialog.setVisible(true);
	}
}
