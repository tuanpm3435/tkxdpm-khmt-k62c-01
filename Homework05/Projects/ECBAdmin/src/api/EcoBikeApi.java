package api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ecb.bean.Bike;
import com.ecb.bean.DockingStation;

public class EcoBikeApi {
	public static final String PATH = "http://localhost:8080/";
	
	private Client client;
	
	private static EcoBikeApi instance;
	
	public static EcoBikeApi getInstance(){
	   if (instance == null){
	       synchronized(EcoBikeApi.class){
	           if (instance == null){
	               instance = new EcoBikeApi();
	       }
	   }
	}
	return instance;
	}
	
	private EcoBikeApi() {
		client = ClientBuilder.newClient();
	}
		
	public ArrayList<DockingStation> getStation(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("stations");
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<DockingStation> res = response.readEntity(new GenericType<ArrayList<DockingStation>>() {});
		return res;
	}
	
	
	public ArrayList<Bike> getBikes(String stationId) {
		WebTarget webTarget = client.target(PATH).path("bikes");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>() {});
		ArrayList<Bike> rs = new ArrayList<Bike>();
		for (int i=0;i<res.size();i++) {
			if (res.get(i).getStationId().equals(stationId)) {
				rs.add(res.get(i));
			}
		}
		System.out.println(res);
		return rs;
	}
	
	public Bike addBike(Bike bike) {
		WebTarget webTarget = client.target(PATH).path("bikes");
		
		String jsonBike = convertToJson(bike);
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON_TYPE);
		Response response = invocationBuilder.post(Entity.entity(jsonBike, MediaType.APPLICATION_JSON));
		
		Bike res = response.readEntity(Bike.class);
		return res;
	}
	public Bike editBike(Bike bike) {
		WebTarget webTarget = client.target(PATH).path("bikes").path(bike.getBikeId());
		String jsonBike= convertToJson(bike);
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON_TYPE);
		Response response = invocationBuilder.post(Entity.entity(jsonBike, MediaType.APPLICATION_JSON));
		Bike res = response.readEntity(Bike.class);
		return res;
	}
	public DockingStation addDockingStation(DockingStation station) {
		WebTarget webTarget = client.target(PATH).path("stations");
		String jsonStation= convertToJson(station);
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON_TYPE);
		Response response = invocationBuilder.post(Entity.entity(jsonStation, MediaType.APPLICATION_JSON));
		DockingStation res = response.readEntity(DockingStation.class);
		return res;
	}
	public DockingStation editDockingStation(DockingStation station) {
		WebTarget webTarget = client.target(PATH).path("stations").path(station.getStationId());
		String jsonStation= convertToJson(station);
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON_TYPE);
		Response response = invocationBuilder.post(Entity.entity(jsonStation, MediaType.APPLICATION_JSON));
		DockingStation res = response.readEntity(DockingStation.class);
		return res;
	}
	private static String convertToJson(Object obj) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
