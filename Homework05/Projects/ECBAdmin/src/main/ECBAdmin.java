package main;

import java.awt.BorderLayout;

import javax.swing.*;

@SuppressWarnings("serial")
public class ECBAdmin extends JFrame {

	public static final int WINDOW_WIDTH = 1200;
	public static final int WINDOW_HEIGHT = 650;
	
	public ECBAdmin(ECBAdminController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);
		
		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);
		
		
		JPanel dockingStationPage = controller.getDockingStationPage();
		tabbedPane.addTab("ManagerDockingStation", null, dockingStationPage, "ManagerDockingStation");
		
		JPanel dvdPage = controller.getBikePage();
		tabbedPane.addTab("ManagerBike", null, dvdPage, "ManagerBike");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Eco Bike for Admin");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ECBAdmin(new ECBAdminController());
			}
		});
	}
}