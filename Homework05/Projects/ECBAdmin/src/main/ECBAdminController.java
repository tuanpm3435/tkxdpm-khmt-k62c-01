package main;

import javax.swing.JPanel;

import com.ecb.controller.AdminBikePageController;
import com.ecb.controller.AdminDockingStationController;

public class ECBAdminController {
	
	public JPanel getBikePage() {
		AdminBikePageController adminBikePageController = new AdminBikePageController();
		return adminBikePageController.getDataPagePane();
	}
	public JPanel getDockingStationPage() {
		AdminDockingStationController controller = new AdminDockingStationController();
		return controller.getDataPagePane();
	}
}
