package test;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ecb.bean.DockingStation;
import com.ecb.dialog.DockingStationDialog;;
public class DialogTest {
	@Test
	public void testDockingStationDialog()
	{
		DockingStation targetStation = new DockingStation();
		targetStation.setStationId("abcxyz");
		DockingStationDialog dialog = new DockingStationDialog(null, targetStation);
		DockingStation testStation =  dialog.getNewData();
		
		assertEquals("error in getNewData DockingStation", targetStation.getStationId(), testStation.getStationId());
		
		
	}
}
