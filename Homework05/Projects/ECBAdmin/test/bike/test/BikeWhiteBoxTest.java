package bike.test;

import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.ecb.bean.Bike;

public class BikeWhiteBoxTest {
private DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	@Test
	public void testcase1 (){	 
		Date date;
		try {
			date = df.parse("16/12/2020 23:11:59");
			Bike bike1 = new Bike("bike1","Xe dap don",5,"99K1-6789",date,"ASAMA",400000,1,"station1",false);
			Bike bike2 = null;
			
			assertTrue("Error", bike1.match(bike2)==true);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Test
	public void testcase2 (){	 
		Date date1, date2;
		try {
			date1 = df.parse("16/12/2020 23:11:59");
			date2 = df.parse("16/12/2020 23:11:59");
			Bike bike1 = new Bike("bike1","Xe dap don",5,"99K1-6789",date1,"ASAMA",400000,1,"station1",false);
			Bike bike2 = new Bike("bike2","Xe dap don",5,"99K1-6789",date1,"ASAMA",400000,1,"station1",false);
			
			assertTrue("Error", bike1.match(bike2)==false);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Test
	public void testcase3 (){	 
		Date date1, date2;
		try {
			date1 = df.parse("16/12/2020 23:11:59");
			date2 = df.parse("16/12/2020 23:11:59");
			Bike bike1 = new Bike("bike1","Xe dap don",5,"99K1-6789",date1,"ASAMA",400000,1,"station1",false);
			Bike bike2 = new Bike("bike1","Xe dap doi",5,"99K1-6789",date2,"ASAMA",400000,1,"station1",false);
			
			assertTrue("Error", bike1.match(bike2)==false);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Test
	public void testcase4 (){	 
		Date date1, date2;
		try {
			date1 = df.parse("16/12/2020 23:11:59");
			date2 = df.parse("16/12/2020 23:11:59");
			Bike bike1 = new Bike("bike1","Xe dap don",5,"99K1-6789",date1,"ASAMA",400000,1,"station1",false);
			Bike bike2 = new Bike("bike1","Xe dap don",7.5,"99K1-6789",date2,"ASAMA",400000,1,"station1",false);
			
			assertTrue("Error", bike1.match(bike2)==false);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Test
	public void testcase5 (){	 
		Date date1, date2;
		try {
			date1 = df.parse("16/12/2020 23:11:59");
			date2 = df.parse("16/12/2020 23:11:59");
			Bike bike1 = new Bike("bike1","Xe dap don",5,"99K1-6789",date1,"ASAMA",400000,1,"station1",false);
			Bike bike2 = new Bike("bike1","Xe dap don",5,"99N1-6789",date2,"ASAMA",400000,1,"station1",false);
			
			assertTrue("Error", bike1.match(bike2)==false);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Test
	public void testcase6 (){	 
		Date date1, date2;
		try {
			date1 = df.parse("16/12/2020 23:11:59");
			date2 = df.parse("17/12/2020 23:11:59");
			Bike bike1 = new Bike("bike1","Xe dap don",5,"99K1-6789",date1,"ASAMA",400000,1,"station1",false);
			Bike bike2 = new Bike("bike1","Xe dap don",5,"99K1-6789",date2,"ASAMA",400000,1,"station1",false);
			
			assertTrue("Error", bike1.match(bike2)==false);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Test
	public void testcase7 (){	 
		Date date1, date2;
		try {
			date1 = df.parse("16/12/2020 23:11:59");
			date2 = df.parse("16/12/2020 23:11:59");
			Bike bike1 = new Bike("bike1","Xe dap don",5,"99K1-6789",date1,"ASAMA",400000,1,"station1",false);
			Bike bike2 = new Bike("bike1","Xe dap don",5,"99K1-6789",date2,"ASAMA 1",400000,1,"station1",false);
			
			assertTrue("Error", bike1.match(bike2)==false);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Test
	public void testcase8 (){	 
		Date date1, date2;
		try {
			date1 = df.parse("16/12/2020 23:11:59");
			date2 = df.parse("16/12/2020 23:11:59");
			Bike bike1 = new Bike("bike1","Xe dap don",5,"99K1-6789",date1,"ASAMA",400000,1,"station1",false);
			Bike bike2 = new Bike("bike1","Xe dap don",5,"99K1-6789",date2,"ASAMA",450000,1,"station1",false);
			
			assertTrue("Error", bike1.match(bike2)==false);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Test
	public void testcase9 (){	 
		Date date1, date2;
		try {
			date1 = df.parse("16/12/2020 23:11:59");
			date2 = df.parse("16/12/2020 23:11:59");
			Bike bike1 = new Bike("bike1","Xe dap don",5,"99K1-6789",date1,"ASAMA",400000,1,"station1",false);
			Bike bike2 = new Bike("bike1","Xe dap don",5,"99K1-6789",date2,"ASAMA",400000,2,"station1",false);
			
			assertTrue("Error", bike1.match(bike2)==false);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Test
	public void testcase10 (){	 
		Date date1, date2;
		try {
			date1 = df.parse("16/12/2020 23:11:59");
			date2 = df.parse("16/12/2020 23:11:59");
			Bike bike1 = new Bike("bike1","Xe dap don",5,"99K1-6789",date1,"ASAMA",400000,1,"station1",false);
			Bike bike2 = new Bike("bike1","Xe dap don",5,"99K1-6789",date2,"ASAMA",400000,1,"station1",false);
			
			assertTrue("Error", bike1.match(bike2)==true);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
