Phân công công việc
1. Phạm Mạnh Tuấn: thiết kế giao diện cho usecase thêm xe
2. Phạm Minh Thùy: thiết kế giao diện cho usecase thêm bãi xe
3. Phương Trung Đức: thiết kế giao diện cho usecase thuê xe
4. Mai Thế Hưng: thiết kế giao diện cho usecase trả xe
5. Nguyễn Văn Huy: thiết kế giao diện cho usecase thanh toán
6. Vũ Văn Quân: thiết kế giao diện cho usecase xem thông tin chi tiết xe
7. Nguyễn Thị Như Hảo: thiết kế giao diện cho usecase xem danh sách bãi xe